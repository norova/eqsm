﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;
using Yourfirefly.EQSM.Properties;

namespace Yourfirefly.EQSM
{
    public partial class FormMain : Form
    {
        public static SettingsLoader settings;
        public Font hotButtonFont;
        public int[] page = {1, 1, 1, 1};
        
        private FormAbout _formAbout;
        
        public string TextEQLocation
        {
            get { return textEQLocation.Text; }
            set { textEQLocation.Text = value; }
        }

        public FormMain()
        {
            // Initialize UI components
            InitializeComponent();

            // Initialize font object
            hotButtonFont = new Font("Arial", 7);

            // Initialize Settings and set this form as the parent for references
            settings = new SettingsLoader { Parent = this };
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            // Display the About form
            _formAbout = new FormAbout();
            _formAbout.ShowDialog(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // Load drive info and scan known directories for EverQuest INI files
            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo d in drives)
                foreach (string l in settings.EQLocations)
                    if (Directory.Exists(d + l))
                        settings.EQLocation = d + l;
            
            // Load all found character INIs
            foreach (Character c in settings.Characters)
                listCharacters.Items.Add(c.Name + " (" + c.Server + ")");
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // Display folder browser dialog so user can select EverQuest directory
            FolderBrowserDialog browser = new FolderBrowserDialog();

            DialogResult result = browser.ShowDialog();
            if (result == DialogResult.OK)
                settings.EQLocation = browser.SelectedPath;
        }

        private void loadHotButtons(Character character, int set = -1)
        {
            if (character == null) throw new ArgumentNullException("character");

            if (set == -1)
            {
                for (int s = 0; s < 4; s++)
                {
                    Label labelPage = new Label
                    {
                        Font =
                            new Font("Microsoft Sans Serif", 6.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte) (0))),
                        Location = new Point(30, 17),
                        Name = "labelPage" + s,
                        Size = new Size(48, 14),
                        TabIndex = 2,
                        Text = page[s].ToString(CultureInfo.InvariantCulture),
                        TextAlign = ContentAlignment.MiddleCenter
                    };

                    PictureBox picRightArrow = new PictureBox
                    {
                        Location = new Point(82, 19),
                        Name = "picRightArrow" + s,
                        Size = new Size(20, 12),
                        TabIndex = 1,
                        TabStop = false,
                        Image = Resources.arrow_right,
                        Tag = s
                    };
                    picRightArrow.Click += picRightArrow_Click;

                    PictureBox picLeftArrow = new PictureBox
                    {
                        Location = new Point(6, 19),
                        Name = "picLeftArrow" + s,
                        Size = new Size(20, 12),
                        TabIndex = 0,
                        TabStop = false,
                        Image = Resources.arrow_left,
                        Tag = s
                    };
                    picLeftArrow.Click += picLeftArrow_Click;

                    GroupBox groupHotButtons = new GroupBox
                    {
                        Location = new Point(212 + ((s)*114), 6),
                        Name = "groupHotButtons" + s,
                        Size = new Size(108, 280),
                        TabIndex = 3,
                        TabStop = false,
                        Text = "Hot Buttons",
                        Visible = false
                    };
                    groupHotButtons.Controls.Add(labelPage);
                    groupHotButtons.Controls.Add(picRightArrow);
                    groupHotButtons.Controls.Add(picLeftArrow);

                    for (int i = 0; i < 10; i++)
                    {
                        PictureBox picBox = new PictureBox
                        {
                            Location = new Point(54 - (((i + 1)%2)*48), 34 + ((i/2)*48)),
                            Height = 48,
                            Width = 48,
                            Name = "button" + i,
                        };

                        Bitmap button = new Bitmap(Resources.hotbutton);
                        Bitmap background = new Bitmap(Resources.hotbutton_empty);
                        Bitmap finalButton = new Bitmap(48, 48);

                        using (Graphics graphics = Graphics.FromImage(finalButton))
                        {
                            graphics.Clear(Color.Transparent);
                            graphics.SmoothingMode = SmoothingMode.HighQuality;
                            graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                            graphics.CompositingQuality = CompositingQuality.HighQuality;

                            graphics.DrawImage(background, 0, 0);

                            if (character.HotButtons.HotButtonPages[(page[s] + (s*10)) - 1].Buttons(i).Value != "")
                                graphics.DrawImage(button, 4, 4);

                            StringFormat stringFormat = new StringFormat(StringFormatFlags.LineLimit);
                            SizeF textSize =
                                graphics.MeasureString(
                                    character.HotButtons.HotButtonPages[(page[s] + (s*10)) - 1].Buttons(i).Value,
                                    hotButtonFont, 36, stringFormat);
                            PointF textLocation = new PointF {X = 24 - (textSize.Width/2), Y = 24 - (textSize.Height/2)};
                            graphics.DrawString(
                                character.HotButtons.HotButtonPages[(page[s] + (s*10)) - 1].Buttons(i).Value,
                                hotButtonFont, Brushes.White, textLocation);
                        }

                        picBox.Image = finalButton;
                        groupHotButtons.Controls.Add(picBox);
                    }

                    tabMain.Controls.Add(groupHotButtons);
                    groupHotButtons.Show();
                }
            }
            else
            {
                Label labelPage = new Label
                {
                    Font = new Font("Microsoft Sans Serif", 6.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))),
                    Location = new Point(30, 17),
                    Name = "labelPage" + set,
                    Size = new Size(48, 14),
                    TabIndex = 2,
                    Text = page[set].ToString(CultureInfo.InvariantCulture),
                    TextAlign = ContentAlignment.MiddleCenter
                };

                PictureBox picRightArrow = new PictureBox
                {
                    Location = new Point(82, 19),
                    Name = "picRightArrow" + set,
                    Size = new Size(20, 12),
                    TabIndex = 1,
                    TabStop = false,
                    Image = Resources.arrow_right,
                    Tag = set
                };
                picRightArrow.Click += picRightArrow_Click;

                PictureBox picLeftArrow = new PictureBox
                {
                    Location = new Point(6, 19),
                    Name = "picLeftArrow" + set,
                    Size = new Size(20, 12),
                    TabIndex = 0,
                    TabStop = false,
                    Image = Resources.arrow_left,
                    Tag = set
                };
                picLeftArrow.Click += picLeftArrow_Click;

                GroupBox groupHotButtons = new GroupBox
                {
                    Location = new Point(212 + ((set) * 114), 6),
                    Name = "groupHotButtons" + set,
                    Size = new Size(108, 280),
                    TabIndex = 3,
                    TabStop = false,
                    Text = "Hot Buttons",
                    Visible = false
                };
                groupHotButtons.Controls.Add(labelPage);
                groupHotButtons.Controls.Add(picRightArrow);
                groupHotButtons.Controls.Add(picLeftArrow);

                for (int i = 0; i < 10; i++)
                {
                    PictureBox picBox = new PictureBox
                    {
                        Location = new Point(54 - (((i + 1) % 2) * 48), 34 + ((i / 2) * 48)),
                        Height = 48,
                        Width = 48,
                        Name = "button" + i,
                    };

                    Bitmap button = new Bitmap(Resources.hotbutton);
                    Bitmap background = new Bitmap(Resources.hotbutton_empty);
                    Bitmap finalButton = new Bitmap(48, 48);

                    using (Graphics graphics = Graphics.FromImage(finalButton))
                    {
                        graphics.Clear(Color.Transparent);
                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                        graphics.CompositingQuality = CompositingQuality.HighQuality;

                        graphics.DrawImage(background, 0, 0);

                        if (character.HotButtons.HotButtonPages[(page[set] + (set * 10)) - 1].Buttons(i).Value != "")
                            graphics.DrawImage(button, 4, 4);

                        StringFormat stringFormat = new StringFormat(StringFormatFlags.LineLimit);
                        SizeF textSize =
                            graphics.MeasureString(
                                character.HotButtons.HotButtonPages[(page[set] + (set * 10)) - 1].Buttons(i).Value,
                                hotButtonFont, 36, stringFormat);
                        PointF textLocation = new PointF { X = 24 - (textSize.Width / 2), Y = 24 - (textSize.Height / 2) };
                        graphics.DrawString(
                            character.HotButtons.HotButtonPages[(page[set] + (set * 10)) - 1].Buttons(i).Value,
                            hotButtonFont, Brushes.White, textLocation);
                    }

                    picBox.Image = finalButton;
                    groupHotButtons.Controls.Add(picBox);
                }

                tabMain.Controls.Add(groupHotButtons);
                groupHotButtons.Show();
            }
        }

        private void listCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            page = new[] {1, 1, 1, 1};

            for (int set = 0; set < 4; set++)
                foreach (GroupBox box in tabMain.Controls.OfType<GroupBox>())
                    if (box.Name == "groupHotButtons" + set)
                        tabMain.Controls.Remove(box);

            loadHotButtons(settings.Characters[listCharacters.SelectedIndex]);
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            hotButtonFont.Dispose();
        }

        private void picLeftArrow_Click(object sender, EventArgs e)
        {
            PictureBox picBox = (PictureBox)sender;
            int set = int.Parse(picBox.Tag.ToString());

            if (page[set] - 1 >= 1)
                page[set]--;
            else
                page[set] = 10;

            foreach (GroupBox box in tabMain.Controls.OfType<GroupBox>())
                if (box.Name == "groupHotButtons" + set)
                    tabMain.Controls.Remove(box);

            loadHotButtons(settings.Characters[listCharacters.SelectedIndex], set);
        }

        private void picRightArrow_Click(object sender, EventArgs e)
        {
            PictureBox picBox = (PictureBox)sender;
            int set = int.Parse(picBox.Tag.ToString());

            if (page[set] + 1 <= 10)
                page[set]++;
            else
                page[set] = 1;

            foreach (GroupBox box in tabMain.Controls.OfType<GroupBox>())
                if (box.Name == "groupHotButtons" + set)
                    tabMain.Controls.Remove(box);

            loadHotButtons(settings.Characters[listCharacters.SelectedIndex], set);
        }
    }
}
